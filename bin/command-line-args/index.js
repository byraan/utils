module.exports = (definitions) => {
	if (!process.argv2) process.argv2 = Object.assign([], process.argv);

	const commandLineArgs = require('command-line-args');
	var result = commandLineArgs(definitions, {
		partial: true,
		camelCase: true
	});

	var i = 0;
	for (; i < process.argv2.length; i++) {
		if (process.argv2[i].substr(0, 1) == '-') break;
		if (result._unknown && result._unknown.indexOf(process.argv2[i]) >= 0) {
			result._command = process.argv2[i];
			break;
		}
	}
	process.argv2.splice(0, i + 1);

	delete result._unknown;
	return result;
};