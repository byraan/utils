#!/usr/bin/env node
process.setMaxListeners(0);
process.emitWarning = (warning, ...args) => {
	if (args[0] === 'ExperimentalWarning') return;
	if (args[0] && typeof args[0] === 'object' && args[0].type === 'ExperimentalWarning') return;
	// return emitWarning(warning, ...args);
};

const path = require('path'),
fs = require('fs');

const commandLineArgs = require('./command-line-args/index.js');
process.commandLineArgs = class {
	constructor(mod, definitions) {
		this.result = commandLineArgs(definitions);
		if (this.command) this.commandPath = mod.path + '/commands/' + this.command + '/index.js';
	}

	get command() {
		return this.result._command;
	}

	runCommand() {
		if (!this.commandPath) throw new Error('Comando requerido');
		if (fs.existsSync(this.commandPath)) return require(this.commandPath);
		throw new Error(`Comando '${this.command}' no encontrado`);
	}
};

// spinner loader
const Spinnies = require('spinnies');
process.loader = new Spinnies();

const notifier = require('node-notifier');
process.notify = (obj, callback) => {
	notifier.notify(obj, function(err, response, metadata) {
		if (typeof callback == 'function') callback.apply(this, [err, response, metadata]);
	});
};

require('../index.js');