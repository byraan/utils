try {
	var cli = new process.commandLineArgs(module);
	cli.runCommand();
}
catch(err) {
	process.loader.add('cli');
	process.loader.fail('cli', {
		text: err.message
	});
}