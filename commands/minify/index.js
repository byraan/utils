const commandLineArgs = new process.commandLineArgs([
	{
		name: 'input',
		alias: 'i',
		type: String,
		defaultValue: ['.'],
		multiple: true
	}, {
		name: 'output',
		alias: 'o',
		type: String
	}, {
		name: 'enclose',
		type: Boolean
	}
]);

const fs = require('fs'),
path = require('path'),
find = require('find');

const CleanCSS = require('clean-css'),
UglifyJS = require("uglify-js");
// const esbuild=require('esbuild');